//find all elements in the DOM
const laptopsElement = document.getElementById("laptops");
const laptopFeatureElement = document.getElementById("features");
const workButtonElement = document.getElementById("work");
const workBalanceElement = document.getElementById("work-balance");
const bankButtonElement = document.getElementById("bank");
const bankBalanceElement = document.getElementById("bank-balance");
const laptopTitleElement = document.getElementById("laptop-title");
const laptopDescriptionElement = document.getElementById("laptop-description");
const laptopPriceElement = document.getElementById("laptop-price");
const laptopImageElement = document.getElementById("laptop-image");
const buyButtonElement = document.getElementById("button-buy");
const loanButtonElement = document.getElementById("button-loan");
const loanInfoElement = document.getElementById("loan-info");
const loanBalanceElement = document.getElementById("loan-balance");
const repayLoanButtonElement = document.getElementById("button-repay");

let laptops = [];

//fetch information from api and store in laptops
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops));

//add information about laptops to menu and initialize elements with first laptop
const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x));
    laptops[0].specs.forEach(x => addLaptopSpecToList(x));
    laptopTitleElement.innerText = laptops[0].title;
    laptopDescriptionElement.innerText = laptops[0].description;
    laptopPriceElement.innerText = laptops[0].price;
    laptopImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptops[0].image;
}

//creates options in select element and adds laptops to it
const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

//changes information based on what laptop is chosen from select menu
const handleLaptopMenuChange = e => {
    laptopFeatureElement.innerHTML = "";
    const selectedLaptop = laptops[e.target.selectedIndex];
    laptopTitleElement.innerText = selectedLaptop.title;
    laptopDescriptionElement.innerText = selectedLaptop.description;
    laptopPriceElement.innerText = selectedLaptop.price;
    laptopImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image;
    selectedLaptop.specs.forEach(x => addLaptopSpecToList(x));
}

//creates a list element for each spec in a laptop
const addLaptopSpecToList = (specs) => {
    const featureElement = document.createElement("li");
    laptopFeatureElement.appendChild(featureElement);
    featureElement.innerHTML = specs;
}

//when the work button is clicked 100 is added to the current value
function clickWorkButton() {
    workBalanceElement.innerHTML = parseInt(workBalanceElement.textContent, 10) + 100;
}

//when the bank button is clicked 
function clickBankButton() {
    let pay = parseInt(workBalanceElement.textContent, 10);
    let bank = parseInt(bankBalanceElement.textContent, 10);
    let loan = parseInt(loanBalanceElement.textContent, 10);
    if (loan > 0) { //if a loan exists
        if (loan <= (pay * 0.1)) { //if you are able to pay off entire loan with 10% of pay
            pay = pay - loan;
            loanBalanceElement.innerHTML = 0;
            loanInfoElement.style.display = "none"; //hide loan elements
            repayLoanButtonElement.style.display = "none"; //hide loan elements
            bankBalanceElement.innerHTML = bank + (pay);
            workBalanceElement.innerHTML = 0;
        }
        else { //pay off some of the loan with 10% of pay and the rest goes to bank
            loanBalanceElement.innerHTML = loan - (pay * 0.1);
            bankBalanceElement.innerHTML = bank + (pay * 0.9);
            workBalanceElement.innerHTML = 0;
        }
    }
    else { //if there is no loan add entire pay to bank and set pay to 0
        bankBalanceElement.innerHTML = parseInt(bankBalanceElement.textContent, 10) + parseInt(workBalanceElement.textContent, 10);
        workBalanceElement.innerHTML = 0;
    }
}
function clickBuyButton() { //when the buy button is clicked 
    if (parseInt(laptopPriceElement.innerHTML, 10) <= parseInt(bankBalanceElement.textContent, 10)) { //check if you have enough money in the bank
        bankBalanceElement.innerHTML = parseInt(bankBalanceElement.textContent, 10) - parseInt(laptopPriceElement.innerHTML, 10); //subtract price of laptop from bank
        alert("Congratulations! You have just bought a " + laptopTitleElement.innerHTML + "."); //tell user they have bought a laptop
    }
    else {
        alert("You do not have sufficient funds in the bank.") //if the user doesnt have money tell them so
    }
}
function clickLoanButton() { //when the loan button is clicked 
    if (loanBalanceElement.innerHTML == 0) {
        let amount = prompt("Enter amount you wish to loan.", "1000"); //prompt for loan amount
        if (amount <= parseInt(bankBalanceElement.textContent, 10) * 2) { //check if loan is less than or equal to twice bank saldo and is an int
            bankBalanceElement.innerHTML = parseInt(bankBalanceElement.textContent, 10) + parseInt(amount, 10); //add loan to bank
            loanInfoElement.style.display = "block"; //display loan info
            repayLoanButtonElement.style.display = "block"; //display loan info
            loanBalanceElement.innerHTML = amount;
        }
        else { //if loan request is twice bank saldo or wrong input
            alert("You have requested a loan of more than twice your bank balance or written a false input. This is not be allowed.")
        }
    }
    else //if you already have a loan
        alert("You already have a loan.");
}

function clickRepayButton() { //when repay button is clicked 
    let pay = parseInt(workBalanceElement.textContent, 10);
    let loan = parseInt(loanBalanceElement.textContent, 10);
    if (loan > pay) { //if you cant pay off entire loan 
        loanBalanceElement.innerHTML = loan - pay;
        workBalanceElement.innerHTML = 0;
    }
    else { //if you can pay off entire loan
        workBalanceElement.innerHTML = pay - loan;
        loanBalanceElement.innerHTML = 0;
        loanInfoElement.style.display = "none";
        repayLoanButtonElement.style.display = "none";
    }
}

//add event listeners to buttons
laptopsElement.addEventListener("change", handleLaptopMenuChange);
workButtonElement.addEventListener("click", clickWorkButton);
bankButtonElement.addEventListener("click", clickBankButton);
buyButtonElement.addEventListener("click", clickBuyButton);
loanButtonElement.addEventListener("click", clickLoanButton);
repayLoanButtonElement.addEventListener("click", clickRepayButton);
