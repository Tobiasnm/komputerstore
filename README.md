# Komputer Store
> Create a Dynamic Webpage using JS

This is assignment 1 of JavaScript Fundamentals in the Java Fullstack course.

## Features
- The bank area where you can store funds and make bank loans
- The work area where you can increase your earnings and deposit cash into your bank balance
- The laptop area where you can choose a laptop and inspect its specs
- The buy area where an image of the laptop is displayed along with a short description and a price

## Usage
Click the work button to add 100 DKK to your pay. When you have money in your pay you can transfer these to the bank with the bank button. 
The loan button will allow you to take a loan of up to twice your bank saldo.
When you have enough money in the bank you can buy a laptop of your choosing. 

### Deployment link

[Link to Heroku deployment](https://komputerstore-tnm.herokuapp.com/)

``` 
Created by Tobias Nordvig Møller
